#!/usr/bin/expect

set timeout 20

set environment   [lindex $argv 0]
set workspace     [lindex $argv 1]
set cacheusername [lindex $argv 2]
set cachepassword [lindex $argv 3]
set branch        [lindex $argv 4]

spawn "./deploy.sh" $environment $workspace $branch

expect "Username:" { send "$cacheusername\r" }
expect "Password:" { send "$cachepassword\r" }

expect "Delete successful" { send_user "Delete succeeded, continuing" }

expect "Username:" { send "$cacheusername\r" }
expect "Password:" { send "$cachepassword\r" }

expect "Load finished successfully." { send_user "Everything loaded successfully, continuing" }

expect "Username:" { send "$cacheusername\r" }
expect "Password:" { send "$cachepassword\r" }

expect {
	"Complete Unit test" { 
		send_user "Everything completed successfully\n"
		exit 0
	}

}