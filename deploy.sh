#!/bin/bash

echo "Environment: $1"
echo "Workspace: $2"

whoami

# Remove previous HealthShare classes
echo "Removing HSTestApi classes...\n"
csession $1 -U HSTESTAPI "##class(%SYSTEM.OBJ).DeletePackage(\"HSTestApi\")"
echo -e "\nDelete successful"

# Deploy HealthShare classes
echo -e "Loading HSTESTAPI classes...\n"
echo -e $2'\n'
csession $1 -U HSTESTAPI "##class(%SYSTEM.OBJ).LoadDir(\"${2}/cls/HSTestApi\",\"ck\",,1)"
echo "\n Start running unit tests...\n"
# Run unit test cases
csession $1 -U HSTESTAPI "##class(HSTestApi.MyTests.RunUnitTest).RunTerminalCommandsForBuild(\"${2}/tests.xml\")"

echo "Complete Unit test"